subroutine CalcstuffBFGS(X,ndvar,fobj,dfdD,fctindx)
  use dimKrig,only:dat
  use dim_opt,only:fcnt,fgcnt,fghcnt,id_proc

  implicit none
  include 'mpif.h'
  integer :: ndvar,i,fctindx
  double precision :: X(ndvar),fobj,dfdD(ndvar)
  double precision::cgrad(int(dat(1)),ndvar),gvec(int(dat(1)))
  character(len=8) :: DBNAME,DBPWD
  character(len=20) :: dirname

  dbname='WINGOPTI'
  dbpwd='KOMAHAN'

  ! write the input file
  call writeX(ndvar+3,3,X)

  ! Execute astros
  call system('python AstrosInputHandler.py')

  fcnt=fcnt+1 ! increase the fct count by 1
  
  if (fctindx.eq.0) then ! call objective fun
     call get_obj(DBNAME,DBPWD,fobj)
     fobj=fobj-2.436d4
     call get_OGRAD(DBNAME,DBPWD,int(dat(1)),ndvar,dfdD)

  else if (fctindx .ne. 0 .and. fctindx .gt.0 .and. fctindx.le.int(dat(1))) then

     call get_con(DBNAME,DBPWD,int(dat(1)),gvec)
     fobj=gvec(fctindx)
     call get_CGRAD(DBNAME,DBPWD,int(dat(1)),ndvar,CGRAD)
     dfdD(1:ndvar)=CGRAD(fctindx,1:ndvar)

  else

     print*,"Wrong function index in CalcStuffBFGS: ",fctindx
     stop

  end if

!  print*,id_proc,fctindx,fobj
!  call system('pwd')

!  call chdir('../../') ! return back to original dir after processing

  return
end subroutine CalcstuffBFGS
!===================

subroutine epigrads(fct,fctindx,dim,ndimt,xtmp,xstdt,ftmp,dftmp)
  use omp_lib

  implicit none
  integer :: DIM,ndimt,fct,fctindx
  real*8,intent(in)  :: xtmp(ndimt),xstdt(ndimt)
  real*8,intent(out) :: dftmp(ndimt)
  real*8::ftmp
  real*8 :: gtol,low(ndimt-DIM),up(ndimt-DIM)

  gtol=1e-6

  ! Epistemic gradients are calculted at the mid point of the interval

  low(1:ndimt-DIM)= xtmp(1:ndimt-DIM)  !+ xstdt(1:ndimt-DIM)
  up(1:ndimt-DIM) = xtmp(1:ndimt-DIM)  !+ xstdt(1:ndimt-DIM)

  call optimize(ndimt-DIM,xtmp,ndimt,ftmp,dftmp,low,up,gtol,.true.,.false.,fctindx)

  return
end subroutine epigrads

!+++++++++++++++++++++++++++++++++++++++++++++++++++++
subroutine writeX(ndimt,ndim,X)
  implicit none

  integer,intent(in)::ndimt,ndim
  integer:: i
  double precision :: X(ndimt)

  !===================================
  ! Write epistemic variables to file
  !===================================

  open(unit=73,file='epistemic.dat')
  do i= 1, ndimt-ndim
!!$     if (i.eq.1) then 
!!$        !0.1-10.0 in
!!$        if (x(i).le.0.1d0) x(i)=0.1d0
!!$        if (x(i).ge.10.0d0) x(i)=10.0d0
!!$     else if (i.eq.2.or. i.eq.3 .or. i.eq.4. .or. i.eq.5 .or. i.eq.10 .or. i.eq.19 .or. i.eq.20 .or. i.eq.21 .or. i.eq.22) then
!!$        !0.25 - 1.5 in
!!$        if (x(i).le.0.25d0) x(i)=0.25d0
!!$        if (x(i).ge.1.5d0) x(i)=1.5d0
!!$     else if (i.eq.6 .or. i.eq.7 .or. i.eq.8 .or. i.eq.9 .or. i.eq.23 .or. i.eq.24 .or. i.eq.25 .or. i.eq.26) then
!!$        !0.1-1.25 in
!!$        if (x(i).le.0.1d0) x(i)=0.1d0
!!$        if (x(i).ge.1.25d0) x(i)=1.25d0
!!$     else if (i.ge.11 .and. i.le.18) then
!!$        !0.1-1.5 in
!!$        if (x(i).le.0.1d0) x(i)=0.1d0
!!$        if (x(i).ge.1.5d0) x(i)=1.5d0
!!$     else
!!$        print*,i
!!$        stop"Wrong I"
!!$     end if
     write(73,'(F17.14)') X(i)
!     write(73,'(F17.12)') X(i)
  end do
  close(73)

  !==================================
  ! Write aleatory variables to file
  !==================================

  open(unit=73,file='aleatory.dat')
  do i= ndimt-ndim+1, ndimt
!     if (i.eq.ndimt-ndim+1)  write(73,'(F10.4)') X(i)/(1.d7)
!     if (i.ne.ndimt-ndim+1)  write(73,'(F14.6)') X(i)
     if (i.eq.ndimt-ndim+1)  write(73,'(F17.12)') X(i)/1.d7
     if (i.ne.ndimt-ndim+1)  write(73,'(F17.12)') X(i)
  end do
  close(73)

  return
end subroutine writeX
!==================================================
subroutine CalcstuffBFGSFull(X,ndvart,M,gvec,CGRAD)

  implicit none

  integer  :: ndvart,fct,i,fctindx,m
  double precision :: X(ndvart),fobj,dfdD(ndvart),x3,dfdDtmp(ndvart-3)
  double precision::cgrad(M,ndvart-3),gvec(M)

  character(len=8) :: DBNAME,DBPWD

  dbname='WINGOPTI'
  dbpwd='KOMAHAN'

  call writeX(ndvart,3,X)
  call system('python AstrosInputHandler.py')

  fobj=0.0d0
  dfdd(:)=0.0d0
 
  call get_con(DBNAME,DBPWD,M,gvec) 
  call get_CGRAD(DBNAME,DBPWD,M,ndvart-3,CGRAD)

  return
end subroutine CalcstuffBFGSFull
  
!=================================
subroutine i_to_s2(intval,s)

  integer idig,intval,ipos,ival,i
  character ( len = * ) s

  s = ''

  ival = intval

  !  Working from right to left, strip off the digits of the integer
  !  and place them into S(1:len ( s )).
  !
  ipos = len(s) 

  do while ( ival /= 0 )

     idig = mod( ival, 10 )
     ival = ival / 10
     s(ipos:ipos) = char(idig + 48 )
     ipos = ipos - 1

  end do
  !
  !  Fill the empties with zeroes.
  !
  do i = 1, ipos
     s(i:i) = '0'
  end do

  return
end subroutine i_to_s2
