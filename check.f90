 PROGRAM main
   use dim_opt,only:id_proc,fcnt,fgcnt,fghcnt,num_proc
   use mpi
!   include 'mpif.h'

   implicit none
!   integer:: num_proc

   call MPI_START
   print*,id_proc,num_proc


   
   call stop_all

 end program main

subroutine epigrads(fct,fctindx,dim,ndimt,xtmp,xstdt,ftmp,dftmp)
  use omp_lib

  implicit none
  integer :: DIM,ndimt,fct,fctindx
  real*8,intent(in)  :: xtmp(ndimt),xstdt(ndimt)
  real*8,intent(out) :: dftmp(ndimt)
  real*8::ftmp
  real*8 :: gtol,low(ndimt-DIM),up(ndimt-DIM)

  gtol=1e-6

  ! Epistemic gradients are calculted at the mid point of the interval

  low(1:ndimt-DIM)= xtmp(1:ndimt-DIM)  !+ xstdt(1:ndimt-DIM)
  up(1:ndimt-DIM) = xtmp(1:ndimt-DIM)  !+ xstdt(1:ndimt-DIM)

!  call optimize(ndimt-DIM,xtmp,ndimt,ftmp,dftmp,low,up,gtol,.true.,.false.,fctindx)

  return
end subroutine epigrads

!+++++++++++++++++++++++++++++++++++++++++++++++++++++
!29,3,X
subroutine writeX(ndimt,ndim,X)
  implicit none

  integer,intent(in)::ndimt,ndim
  integer:: i
  double precision :: X(ndimt)

  !===================================
  ! Write epistemic variables to file
  !===================================

  open(unit=73,file='epistemic.dat')
  do i= 1, ndimt-ndim
     if (i.eq.1) then 
        !0.1-1.0 in
        if (x(i).le.0.1d0) x(i)=0.1d0
        if (x(i).ge.1.0d0) x(i)=1.0d0
     else if (i.eq.2.or. i.eq.3 .or. i.eq.4. .or. i.eq.5 .or. i.eq.10 .or. i.eq.19 .or. i.eq.20 .or. i.eq.21 .or. i.eq.22) then
        !0.25 - 1.5 in
        if (x(i).le.0.1d0) x(i)=0.25d0
        if (x(i).ge.1.0d0) x(i)=1.5d0
     else if (i.eq.6 .or. i.eq.7 .or. i.eq.8 .or. i.eq.9 .or. i.eq.23 .or. i.eq.24 .or. i.eq.25 .or. i.eq.26) then
        !0.1-1.25 in
        if (x(i).le.0.1d0) x(i)=0.1d0
        if (x(i).ge.1.0d0) x(i)=1.25d0
     else if (i.ge.11 .and. i.le.18) then
        !0.1-1.5 in
        if (x(i).le.0.1d0) x(i)=0.1d0
        if (x(i).ge.1.0d0) x(i)=1.5d0
     else
        print*,i
        stop"Wrong I"
     end if
     write(73,'(F17.14)') X(i)
  end do
  close(73)

  !==================================
  ! Write aleatory variables to file
  !==================================

  open(unit=73,file='aleatory.dat')
  do i= ndimt-ndim+1, ndimt
     if (i.eq.ndimt-ndim+1)  write(73,'(F10.4)') X(i)/(1.d7)
     if (i.ne.ndimt-ndim+1)  write(73,'(F14.6)') X(i)
  end do
  close(73)

  return
end subroutine writeX
