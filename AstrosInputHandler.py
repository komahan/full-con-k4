'''
Module to do file handling with ASTROS.
Gets the uncertain values (design variables and other parameters) and updates
them in the input file.
The aleatory variables are supplied as aleatory.inp.
The epistemic variables are supplied as epistemic.inp.
'''

import shutil
import os
import commands

__author__ = "komahan"
__date__ = "$May 5, 2014 2:16:54 PM$"

if __name__ == "__main__":
    pass
    #    print("""Executing program to amend the astros input based on the supplied data:
    #   (1) epistemic.dat
    #   (2) aleatory.dat""")

#inputfile = "threebaropt.inp" # Name of the input file (pass as input)
#inputfile = "tenbar.inp" # Name of the input file (pass as input)
inputfile = "wingopt.inp" # Name of the input file (pass as input)

    # Make copy of the input file
def makeCopy(myfile):
    """
    Method to make a new copy of the input file and use it for writing
    This is to prevent accidental file damage in case of an error.
    """
    newfile = myfile + ".temp"
    try:
        shutil.copyfile(myfile, newfile)
    except:
        raise
    return newfile
 

# Read the input file
def readInputFile(filename):
    """
    Method to read the input file and return the data as string.
    """
    try:
        inpFile = open(filename, "r")
        content = list(inpFile.readlines())
        inpFile.close()
    except:
        raise IOError
    return content
# Write the data to the input file
def writeInputFile(filename,filecontent):
    """
    Method to write the final data to the file. If success it writes to the original file.
    If failed to write for some reason, the original input file is untouched
    """
    try:
        outFileName=makecopy(filename)
        outFile = open(outFileName, "w")
        outFile.writelines(filecontent)
        outFile.close()
    except:
        raise
    #print(type(outFile),type(filename))
    #print(outFileName,filename)
    shutil.copyfile(outFileName,filename)

# Read the epistemic input variables

def readEpistemic():
    f=open("epistemic.dat")
    deselm=f.readlines()
    f.close()
    return deselm

def readAleatory():
    f=open("aleatory.dat")
    alvars=f.readlines()
    f.close()
    return alvars



outFileName=makeCopy(inputfile)

# Checks if exist and remove the old files
if os.path.isfile(outFileName):
    os.remove(outFileName)

if os.path.isfile(inputfile+"ut"):
    os.remove(inputfile+"ut")

# Initialize
splitlist = [];newcontentlist = []; cnt=-1;

outfile2=open(outFileName,"w")
# Gather the contents to operate with
contentlist = readInputFile(inputfile)
deselm=readEpistemic() # read the epistemic into file
alvars=readAleatory()  # read aleatory into file

for item in contentlist:
    splititem = item.split(',')   
    try:
        indx2 = splititem.index("MAT1*")
    except ValueError:
        indx2 = -1
    if indx2 == 0:
        splititem[2] = str(alvars[0]).strip()+"D7   "
        splititem[4] = str(alvars[1]).strip()+"   "
        splititem[5] = str(alvars[2]).strip()
    try:
        indx = splititem.index("DESVARP*")
    except ValueError:
        indx = -1
    if indx == 0:
        cnt=cnt+1
        splititem[3] = str(deselm[cnt]).strip()
        splititem[4] = str(deselm[cnt]).strip()
        splititem[5] = str(deselm[cnt]).strip()
    for index, entry in enumerate(splititem):
        if index!=0:
            outfile2.write(",  "+entry)
        else:
            outfile2.write(entry)
outfile2.close()

# If everything successful

shutil.move(outFileName,inputfile+"ut")

## Run ASTROS with this input file

commands.getstatusoutput('./astros.bin -m 2P <wingopt.input >out')
