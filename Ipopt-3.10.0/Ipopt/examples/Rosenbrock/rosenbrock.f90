! Copyright (C) 2002, 2010 Carnegie Mellon University and others.
! All Rights Reserved.
! This code is published under the Eclipse Public License.
!
!    $Id: hs071_f.f.in 1861 2010-12-21 21:34:47Z andreasw $
!
! =============================================================================
!
!
!  This file contains routines to define a small Rosenbrock test problem.
!
!  f=0 at the optimal solution for x_i=1 for all i!
!
! =============================================================================
!
!
! =============================================================================
!
!                            Main driver program
!
! =============================================================================
!
      program example
!
      implicit none
!
!     include the Ipopt return codes
!
      include 'IpReturnCodes.inc'
!
!     Size of the problem (number of variables and equality constraints)
!
      integer     N,     M,     NELE_JAC,     NELE_HESS,      IDX_STY
      parameter  (N = 2, M = 0, NELE_JAC = 0, NELE_HESS = 3)
      parameter  (IDX_STY = 1 )
!
!     Space for multipliers and constraints
!
      double precision LAM(M)
      double precision G(M)
!
!     Vector of variables
!
      double precision X(N)
!
!     Vector of lower and upper bounds
!
      double precision X_L(N), X_U(N), Z_L(N), Z_U(N)
      double precision G_L(M), G_U(M)
!
!     Private data for evaluation routines
!     This could be used to pass double precision and integer arrays untouched
!     to the evaluation subroutines EVAL_*
!
      double precision DAT(2)
      integer IDAT(1)
!
!     Place for storing the Ipopt Problem Handle
!
      integer*8 IPROBLEM
      integer*8 IPCREATE
!
      integer IERR
      integer IPSOLVE, IPADDSTROPTION
      integer IPADDNUMOPTION, IPADDINTOPTION
      integer IPOPENOUTPUTFILE
!
      double precision F
      integer i
!
!     The following are the Fortran routines for computing the model
!     functions and their derivatives - their code can be found further
!     down in this file.
!
      external EV_F, EV_G, EV_GRAD_F, EV_JAC_G, EV_HESS
!!
!!     The next is an optional callback method.  It is called once per
!!     iteration.
!!
!      external ITER_CB
!
!     Set initial point and bounds:
!
      data X   / -1.2d0, 1.d0 /
      data X_L / -1d1, -1d1 /
      data X_U / 1d1, 1d1 /
!
!     Set bounds for the constraints
!
      data G_L / 25d0, 40d0 /
      data G_U / 1d40, 40d0 /
!
!     First create a handle for the Ipopt problem (and read the options
!     file)
!
      IPROBLEM = IPCREATE(N, X_L, X_U, M, G_L, G_U, NELE_JAC, NELE_HESS,IDX_STY, EV_F, EV_G, EV_GRAD_F, EV_JAC_G, EV_HESS)
      if (IPROBLEM.eq.0) then
         write(*,*) 'Error creating an Ipopt Problem handle.'
         stop
      endif
!
!     Open an output file
!
      IERR = IPOPENOUTPUTFILE(IPROBLEM, 'IPOPT.OUT', 5)
      if (IERR.ne.0 ) then
         write(*,*) 'Error opening the Ipopt output file.'
         goto 9000
      endif
!

!!
!!     Set a callback function to give you control once per iteration.
!!     You can use it if you want to generate some output, or to stop
!!     the optimization early.
!!
!      call IPSETCALLBACK(IPROBLEM, ITER_CB)

!
!     As a simple example, we pass the constants in the constraints to
!     the EVAL_C routine via the "private" DAT array.
!
      DAT(1) = 0.d0
      DAT(2) = 0.d0
!
!     Call optimization routine
!

      IERR = IPSOLVE(IPROBLEM, X, G, F, LAM, Z_L, Z_U, IDAT, DAT)
!
!     Output:
!
      if( IERR.eq.IP_SOLVE_SUCCEEDED ) then
         write(*,*)
         write(*,*) 'The solution was found.'
         write(*,*)
         write(*,*) 'The final value of the objective function is ',F
         write(*,*)
         write(*,*) 'The optimal values of X are:'
         write(*,*)
         do i = 1, N
            write(*,*) 'X  (',i,') = ',X(i)
         enddo
         write(*,*)
         write(*,*) 'The multipliers for the lower bounds are:'
         write(*,*)
         do i = 1, N
            write(*,*) 'Z_L(',i,') = ',Z_L(i)
         enddo
         write(*,*)
         write(*,*) 'The multipliers for the upper bounds are:'
         write(*,*)
         do i = 1, N
            write(*,*) 'Z_U(',i,') = ',Z_U(i)
         enddo
         write(*,*)
         write(*,*) 'The multipliers for the equality constraints are:'
         write(*,*)
         do i = 1, M
            write(*,*) 'LAM(',i,') = ',LAM(i)
         enddo
         write(*,*)
      else
         write(*,*)
         write(*,*) 'An error occoured.'
         write(*,*) 'The error code is ',IERR
         write(*,*)
      endif
!
 9000 continue
!
!     Clean up
!
      call IPFREE(IPROBLEM)
      stop
!
 9990 continue
      write(*,*) 'Error setting an option'
      goto 9000
      end
!
! =============================================================================
!
!                    Computation of objective function
!
! =============================================================================
!
      subroutine EV_F(N, X, NEW_X, F, IDAT, DAT, IERR)
      implicit none
      integer N, NEW_X,I
      double precision F, X(N)
      double precision DAT(*)
      integer IDAT(*)
      integer IERR

      F=0.d0
      do i=1,N-1
         F=F+100.d0*(X(i+1)-X(i)**2)**2+(1-X(i))**2
      end do
      IERR = 0
      return
      end
!
! =============================================================================
!
!                Computation of gradient of objective function
!
! =============================================================================
!
      subroutine EV_GRAD_F(N, X, NEW_X, GRAD, IDAT, DAT, IERR)
      implicit none
      integer N, NEW_X,i
      double precision GRAD(N), X(N)
      double precision DAT(*)
      integer IDAT(*)
      integer IERR

      GRAD(1)=-200.d0*(x(2)-x(1)**2)*2.d0*x(1)-2.d0*(1-x(1))
      do i=2,N-1
         GRAD(i)=200.d0*(x(i)-x(i-1)**2)-200.d0*(x(i+1)-x(i)**2)*2.d0*x(i)-2.d0*(1-x(i))
      end do
      GRAD(n)=200.d0*(x(n)-x(n-1)**2)
      IERR = 0
      return
      end
!
! =============================================================================
!
!                     Computation of equality constraints
!
! =============================================================================
!
      subroutine EV_G(N, X, NEW_X, M, G, IDAT, DAT, IERR)
      implicit none
      integer N, NEW_X, M
      double precision G(M), X(N)
      double precision DAT(*)
      integer IDAT(*)
      integer IERR
!      G(1) = X(1)*X(2)*X(3)*X(4) - DAT(1)
!      G(2) = X(1)**2 + X(2)**2 + X(3)**2 + X(4)**2 - DAT(2)
      IERR = 0
      return
      end
!
! =============================================================================
!
!                Computation of Jacobian of equality constraints
!
! =============================================================================
!
      subroutine EV_JAC_G(TASK, N, X, NEW_X, M, NZ, ACON, AVAR, A,IDAT, DAT, IERR)
      integer TASK, N, NEW_X, M, NZ
      double precision X(N), A(NZ)
      integer ACON(NZ), AVAR(NZ), I
      double precision DAT(*)
      integer IDAT(*)
      integer IERR
!
!     structure of Jacobian:
!
      integer AVAR1(8), ACON1(8)
      data  AVAR1 /1, 2, 3, 4, 1, 2, 3, 4/
      data  ACON1 /1, 1, 1, 1, 2, 2, 2, 2/
      save  AVAR1, ACON1
!
!$$$      if( TASK.eq.0 ) then
!$$$        do I = 1, 8
!$$$          AVAR(I) = AVAR1(I)
!$$$          ACON(I) = ACON1(I)
!$$$        enddo
!$$$      else
!$$$        A(1) = X(2)*X(3)*X(4)
!$$$        A(2) = X(1)*X(3)*X(4)
!$$$        A(3) = X(1)*X(2)*X(4)
!$$$        A(4) = X(1)*X(2)*X(3)
!$$$        A(5) = 2d0*X(1)
!$$$        A(6) = 2d0*X(2)
!$$$        A(7) = 2d0*X(3)
!$$$        A(8) = 2d0*X(4)
!$$$      endif
      IERR = 0
      return
      end
!
! =============================================================================
!
!                Computation of Hessian of Lagrangian
!
! =============================================================================
!
      subroutine EV_HESS(TASK, N, X, NEW_X, OBJFACT, M, LAM, NEW_LAM,NNZH, IRNH, ICNH, HESS, IDAT, DAT, IERR)
      implicit none
      integer TASK, N, NEW_X, M, NEW_LAM, NNZH, i, ir
      double precision X(N), OBJFACT, LAM(M), HESS(NNZH)
      integer IRNH(NNZH), ICNH(NNZH)
      double precision DAT(*)
      integer IDAT(*)
      integer IERR


      if( TASK.eq.0 ) then
!
!     structure of sparse Hessian (lower triangle):
!
         do i = 1, N
            IRNH(i) = i
            ICNH(i) = i
         enddo
         ir=n+1
         do i=1,n-1
            IRNH(ir) = i+1
            ICNH(ir) = i
            ir=ir+1
         end do

      else
!
!     calculate Hessian:
!
         do i = 1, NNZH
            HESS(i) = 0.d0
         enddo
!
!     objective function
!

         hess(1)=-400.d0*x(2)+1200.d0*x(1)**2+2.d0
         do i=2,n-1
            hess(i)=200.d0-400.d0*x(i+1)+1200.d0*x(i)**2+2.d0
         end do
         hess(n)=200.d0
     
         ir=n+1
         do i=1,n-1
            hess(ir)=-400.d0*x(i) 
            ir=ir+1
         end do

         do i = 1, NNZH
            HESS(i) = OBJFACT * HESS(i)
         enddo

!$$$!
!$$$!     first constraint
!$$$!
!$$$         HESS(2) = HESS(2) + LAM(1) * X(3)*X(4)
!$$$         HESS(4) = HESS(4) + LAM(1) * X(2)*X(4)
!$$$         HESS(5) = HESS(5) + LAM(1) * X(1)*X(4)
!$$$         HESS(7) = HESS(7) + LAM(1) * X(2)*X(3)
!$$$         HESS(8) = HESS(8) + LAM(1) * X(1)*X(3)
!$$$         HESS(9) = HESS(9) + LAM(1) * X(1)*X(2)
!$$$!
!$$$!     second constraint
!$$$!
!$$$         HESS(1) = HESS(1) + LAM(2) * 2d0
!$$$         HESS(3) = HESS(3) + LAM(2) * 2d0
!$$$         HESS(6) = HESS(6) + LAM(2) * 2d0
!$$$         HESS(10)= HESS(10)+ LAM(2) * 2d0

      endif
      IERR = 0
      return
      end
!
! =============================================================================
!
!                   Callback method called once per iteration
!
! =============================================================================
!
      subroutine ITER_CB(ALG_MODE, ITER_COUNT,OBJVAL, INF_PR, INF_DU,MU, DNORM, REGU_SIZE, ALPHA_DU, ALPHA_PR, LS_TRIAL, IDAT,DAT, ISTOP)
      implicit none
      integer ALG_MODE, ITER_COUNT, LS_TRIAL
      double precision OBJVAL, INF_PR, INF_DU, MU, DNORM, REGU_SIZE
      double precision ALPHA_DU, ALPHA_PR
      double precision DAT(*)
      integer IDAT(*)
      integer ISTOP
!
!     You can put some output here
!
      write(*,*) 'Testing callback function in iteration ', ITER_COUNT
!
!     And set ISTOP to 1 if you want Ipopt to stop now.  Below is just a
!     simple example.
!
      if (INF_PR.le.1D-04) ISTOP = 1

      return
      end
