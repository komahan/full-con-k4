program problemKriging
  use dim_opt,only:id_proc,fcnt,fgcnt,fghcnt

  implicit none
  !
  !     include the Ipopt return codes
  !
  include 'IpReturnCodes.inc'
  include 'mpif.h'
  !
  !     Size of the problem (number of variables and equality constraints)
  !
  integer     N,     M,     NELE_JAC,     NELE_HESS,      IDX_STY
  parameter  (N = 26+3, M = 68, NELE_JAC =68*(26+3), NELE_HESS = 29*30/2)
  parameter  (IDX_STY = 1 )

  EXTERNAL     DBBD
  EXTERNAL     XXBD
  integer::length
  COMMON /MEMLEN/ LENGTH
  character(len=8) :: DBNAME,DBPWD
  !
  !     Space for multipliers and constraints
  !
  double precision LAM(M)
  double precision G(M)
  !
  !     Vector of variables
  !

  double precision X(N)
  !
  !     Vector of lower and upper bounds
  !
  double precision X_L(N), X_U(N), Z_L(N), Z_U(N)
  double precision G_L(M), G_U(M)
  !
  !     Private data for evaluation routines
  !     This could be used to pass double precision and integer arrays untouched
  !     to the evaluation subroutines EVAL_*
  !
  double precision DAT(4*n+2+(n*m)+20+(m+1)+(m+1)+(m+1))
  integer IDAT(10)

  !
  !     Place for storing the Ipopt Problem Handle
  !

  integer*8 IPROBLEM
  integer*8 IPCREATE
  integer IERR
  integer IPSOLVE, IPADDSTROPTION
  integer IPADDNUMOPTION, IPADDINTOPTION
  integer IPOPENOUTPUTFILE
  double precision F,Fs,sigmax(N),pi
  integer i,kprob
  double precision  infbound

  parameter        (infbound = 1.d+20)
  external EV_F, EV_G, EV_GRAD_F, EV_JAC_G, EV_HESS, ITER_CB

  
!!$  1 to N                                  sigma
!!$  n+1                                     fmeantmp
!!$  n+2                                     fvartmp
!!$  n+2+1 to n+2+N                          fmeanprimetmp+fvarprimetmp
!!$  n+2+n+1 to n+2+n+n                      xsave
!!$  n+2+n+n+1 to n+2+n+n+(n*m)              dc
!!$  n+2+n+n+(n*m)+1 to  n+2+n+n+(n*m)+20    dat array for surrogate

  call MPI_START

  call mminit(length)

  dbname='WINGOPTI'
  dbpwd='KOMAHAN'


  !======================================
  !(1)    Set initial point and bounds:
  !=======================================

  ! Evals counter initialization

  fcnt   =0
  fgcnt  =0
  fghcnt =0

  X_L(1) = 0.10d0

  X_L(2) = 0.25d0
  X_L(3) = 0.25d0
  X_L(4) = 0.25d0
  X_L(5) = 0.25d0

  X_L(6) = 0.1d0
  X_L(7) = 0.1d0
  X_L(8) = 0.1d0
  X_L(9) = 0.1d0

  X_L(10) = 0.25d0

  X_L(11) = 0.1d0
  X_L(12) = 0.1d0
  X_L(13) = 0.1d0
  X_L(14) = 0.1d0
  X_L(15) = 0.1d0
  X_L(16) = 0.1d0
  X_L(17) = 0.1d0
  X_L(18) = 0.1d0

  X_L(19) = 0.25d0
  X_L(20) = 0.25d0
  X_L(21) = 0.25d0
  X_L(22) = 0.25d0

  X_L(23) = 0.1d0
  X_L(24) = 0.1d0
  X_L(25) = 0.1d0
  X_L(26) = 0.1d0


  X_U(1) = 10.0d0
  X_U(2) = 1.5d0
  X_U(3) = 1.5d0
  X_U(4) = 1.5d0
  X_U(5) = 1.5d0

  X_U(6) = 1.25d0
  X_U(7) = 1.25d0
  X_U(8) = 1.25d0
  X_U(9) = 1.25d0

  X_U(10) = 1.5d0

  X_U(11) = 1.5d0
  X_U(12) = 1.5d0
  X_U(13) = 1.5d0
  X_U(14) = 1.5d0
  X_U(15) = 1.5d0
  X_U(16) = 1.5d0
  X_U(17) = 1.5d0
  X_U(18) = 1.5d0

  X_U(19) = 1.5d0
  X_U(20) = 1.5d0
  X_U(21) = 1.5d0
  X_U(22) = 1.5d0

  X_U(23) = 1.25d0
  X_U(24) = 1.25d0
  X_U(25) = 1.25d0
  X_U(26) = 1.25d0

  do i=1,N
     X(i)   = (x_l(i)+x_u(i))/2.0d0
  end do

  !======================
  ! Aleatory variables 
  !======================

  X(N-2)=1.D+7
  X(N-1)=0.33d0
  X(N)=0.10d0

  X_L(N-2)=1.D+7
  X_L(N-1)=0.33d0
  X_L(N)=0.10d0

  X_U(N-2)=1.D+7
  X_U(N-1)=0.33d0
  X_U(N)=0.10d0

  !===================================================================
  !(2)     Integer Settings and store into IDAT (check for size above)
  !===================================================================

  !  probtype(1:N)=1

  kprob=4

  IDAT(1)=kprob
  IDAT(2)=0
  IDAT(3)=M
  IDAT(4)=4*n+2+(n*m)         ! pointer to last index that takes part in optimization
  idat(5)=idat(4)
  IDAT(6)=IDAT(5) + 20        ! pointer to data for kriging data input IDAT(4)+1:IDAT(5)
  IDAT(7)=IDAT(6) + m+1       ! index pointer for f/g values
  IDAT(8)=IDAT(7) + m+1       ! index pointer for f/g evals
  IDAT(9)=IDAT(8) + m+1       ! index pointer for wall time

  ! IDAT(3:N+2)=probtype(1:N)

  !===============================================
  !(3)     Setup std dev and store in to dat(1:N)
  !===============================================

  do i=1,N-3
     sigmax(i)=0.05d0 ! Epistemic uncertainties
  end do

  ! Aleatory uncertainties

  sigmax(N-2) = 2.5D4  ! Youngs modulus (uniform)
  sigmax(N-1) = 0.033D0 ! Poisson Ratio (uniform)
  sigmax(N)   = 0.003D0 ! density (normal)

  ! Storing the uncertainties within DAT array

  dat(:)=0.0 ! initialize

  do i=1,N
     dat(i)=sigmax(i)
  end do

  !====================
  !(4)     Constraints
  !====================

  do i=1,M
     G_L(i)=-infbound
     G_U(i)=0.d0
  end do

  !===========================================================
  !(5)    Other constants to be passed to the surrogate call
  !===========================================================

  dat(4*n+2+(n*m)+ 1) = M           ! Num. of cons for use in calcstuffbfgs
  dat(4*n+2+(n*m)+14) = 1.00d0      ! Factor of safety
  dat(4*n+2+(n*m)+20) = 99          ! Filenum for kriging output

  !=====================================================================
  !
  !     First create a handle for the Ipopt problem (and read the options
  !     file)
  !======================================================================

  IPROBLEM = IPCREATE(N, X_L, X_U, M, G_L, G_U, NELE_JAC, NELE_HESS,IDX_STY, EV_F, EV_G, EV_GRAD_F, EV_JAC_G, EV_HESS)

  if (IPROBLEM.eq.0) then
     write(*,*) 'Error creating an Ipopt Problem handle.'
     call stop_all
  endif

  !==========================
  !     Open an output file
  !==========================

  if (id_proc.eq.0) open(unit=716,file='opt.his',form='formatted',status='replace')

  if (id_proc.eq.0) open(unit=816,file='beta.his',form='formatted',status='replace')

  IERR = IPOPENOUTPUTFILE(IPROBLEM, 'IPOPT.OUT', 5)

  if (IERR.ne.0 ) then
     write(*,*) 'Error opening the Ipopt output file.'

     goto 9000
  endif

  !!====================================================================
  !!     Set a callback function to give you control once per iteration.
  !!     You can use it if you want to generate some output, or to stop
  !!     the optimization early.
  !!====================================================================

  call IPSETCALLBACK(IPROBLEM, ITER_CB)

  !===============================
  !     Call optimization routine
  !===============================

  if (id_proc.eq.0) then
     IERR = IPADDINTOPTION(IPROBLEM, 'print_level', 0)
     if (IERR.ne.0 ) goto 9990
  else
     IERR = IPADDINTOPTION(IPROBLEM, 'print_level', 0)
     if (IERR.ne.0 ) goto 9990
  end if

  IERR = IPSOLVE(IPROBLEM, X, G, F, LAM, Z_L, Z_U, IDAT, DAT)

  !===============
  !     Output:
  !===============

  if (id_proc.eq.0) then ! print the result

     if( IERR.eq.IP_SOLVE_SUCCEEDED .or. IERR.eq.5) then
        write(*,*)
        write(*,*) 'The solution was found.'
        write(*,*)
     else
        write(*,*)
        write(*,*) 'An error occoured.'
        write(*,*) 'The error code is ',IERR
        write(*,*)
     endif

     write(*,*) 'The final value of the objective function is ',F,'lb'
     write(*,*)
     write(*,*) 'The optimal values of X are:'
     write(*,*)
     do i = 1, N
        write(*,*) 'X  (',i,') = ',X(i)
     enddo
     write(*,*)
     write(*,*) 'The multipliers for the equality constraints are:'
     write(*,*)

     do i = 1, M
        write(*,*) 'LAM(',i,') = ',LAM(i)
     end do

     write(*,*)
     write(*,'(a,4F13.6)') 'Weight, variance, SD, CV:',DAT(N+1),DAT(N+2),sqrt(DAT(N+2)),sqrt(DAT(N+2))/DAT(N+1)

  end if
  !
9000 continue
  !=================
  !     Clean up
  !=================

  call IPFREE(IPROBLEM)
  if (id_proc.eq.0) close(716)
  if (id_proc.eq.0) close(816)
  call stop_all
  !
9990 continue
  write(*,*) 'Error setting an option'
  goto 9000

end program problemKriging
!
! =============================================================================
!
!                    Computation of objective function
!
! =============================================================================
!

subroutine EV_F(N, X, NEW_X, F, IDAT, DAT, IERR)
  use dim_opt,only:id_proc,fcnt,fgcnt,fghcnt
  use timer_mod

  implicit none
  include 'mpif.h'
  integer N, NEW_X,I
  double precision::f, X(N),sigmax(N),xsave(n)
  double precision::fmeantmp,fvartmp
  double precision::fmeanprimetmp(n),fvarprimetmp(n)
  double precision::fmeandbleprimetmp(n,n),fvardbleprimetmp(n,n)
  double precision DAT(*)
  real::tt
  integer IDAT(*),kprob
  integer IERR
  integer::myflag(10) 
  integer::M

  M=idat(3)
  kprob=IDAT(1)

  do i=1,N
     sigmax(i)=DAT(i) !take the uncertainties back from dat
     Xsave(i)=X(i)    !save the current X
  end do

  !---- MEAN and VARIANCE OF worst OBJECTIVE FUNCTION

  if (id_proc.eq.0) then

     tt=0.0
     fcnt=0

     call TimerInit()
     call TimerStart('Build Kriging START')
     call Krigingestimate(3,N,x,sigmax,24,0,dat(IDAT(5)+1:IDAT(6)),20,20,20,0,myflag,fmeantmp,fvartmp,fmeanprimetmp,fvarprimetmp)
     call TimerStop('Build Kriging END')
     call sendtime(tt)

     call system('mv HISTGidx000.dat krig/.')

     dat(idat(6)+1)=fmeantmp+fvartmp
     dat(idat(7)+1)=fcnt
     dat(idat(8)+1)=tt

  end if

  call MPI_BCAST(dat(idat(6)+1),1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(dat(idat(7)+1),1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(dat(idat(8)+1),1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

  call MPI_BCAST(fmeantmp,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(fvartmp,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(fmeanprimetmp,N,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(fvarprimetmp,N,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

  if (IDAT(2).eq.1) then ! Deterministic with kriging
     fvartmp=0.0d0
     fvarprimetmp=0.0d0
  end if

  !---- COMBINED OBJECTIVE FUNCTION
  F=fmeantmp+fvartmp

  DAT(N+1)=fmeantmp
  DAT(N+2)=fvartmp

  do i=1,N
     DAT(N+2+i)= fmeanprimetmp(i)+fvarprimetmp(i)
  end do


  if (id_proc.eq.0) then
     print*,''
     write(*,'(4x,a,3F13.4,F8.1,i8)') '>>Objective:',fmeantmp,fvartmp,fmeantmp+fvartmp,dat(idat(8)+1),int(DAT(IDAT(7)+1))
     write(*,'(4x,a,F13.6)') '>>Coeff of variance :',sqrt(fvartmp)/fmeantmp
  end if

  do i=1,n
     DAT(2*N+2+i)=Xsave(i)
     X(i)=Xsave(i)
  end do

  IERR = 0
  return

end subroutine EV_F

!
! =============================================================================
!
!                     Computation of constraints
!
! =============================================================================
!
subroutine EV_G(N, X, NEW_X, M, G, IDAT, DAT, IERR)
  use dim_opt,only:id_proc,fcnt,fgcnt,fghcnt,num_proc
  use timer_mod

  implicit none
  include 'mpif.h'
  integer N, NEW_X, M
  double precision :: X(N), sigmax(N), cmean(M), cstd(M), fmeantmp, fvartmp
  double precision DAT(*),fmeanprimetmp(m,n),fvarprimetmp(m,n),dc(M,N)
  real*8 :: fmeandbleprimetmp(n,n),fvardbleprimetmp(n,n)
  integer IDAT(*),kprob,NMC
  integer IERR, i, j, cnt
  double precision fmin,fmax,gradmin(N-1),gradmax(N-1),gtol,low(N-1),up(N-1),Xsave(N)
  integer::myflag(10) 
  real::tt
  double precision ::gvec(m),cgrad(m,n-3)
  double precision :: G(M)
  character(len=11)::chdirec
  integer :: idx,is,ie,idec,id
  character(len=3)::iNumber

  kprob=IDAT(1)

  do i=1,N
     sigmax(i)=DAT(i)
     Xsave(i)=X(i)
  end do

  dc(:,:)=0.0

  !====================================================!
  !================ WORK SHARE ========================!
  !====================================================!

  idec = dble(M)/dble(num_proc)
  is   = idec*id_proc + 1
  ie   = idec*(id_proc+1)
  if(id_proc.eq.num_proc-1)ie = M

  chdirec(1:8)="krig/con"

  do i=is,ie
    
     call i_to_s2(i,Inumber)
     chdirec(9:11)=inumber
     call chdir(chdirec)

     tt=0.0
     fcnt=0

     call TimerInit()
     call TimerStart('Build Kriging START')        
     call Krigingestimate(3,N,x,sigmax,24,i,dat(IDAT(5)+1:IDAT(6)),20,20,20,0,myflag,fmeantmp,fvartmp,fmeanprimetmp(i,1:n),fvarprimetmp(i,1:n))
     call TimerStop('Build Kriging END')
     call sendtime(tt)

     call system('mv HIST* ../.')
     call chdir('../../')

     DAT(IDAT(6)+1+i) = fmeantmp+dble(kprob)*sqrt(fvartmp)
     DAT(IDAT(7)+1+i) = fcnt ! store the # fn evals
     DAT(IDAT(8)+1+i) = tt   ! store the time

     if (fvartmp.lt.0.0) fvartmp=0.0
     if (IDAT(2).eq.1) then
        fvartmp=0.0
        fvarprimetmp(i,:)=0.0
     end if
     cmean(i)=fmeantmp
     cstd(i)=sqrt(fvartmp)

     write(*,'(2i4,3E15.6,F15.6,F8.1,i8)'),i,id_proc,cmean(i),cstd(i),DAT(IDAT(6)+1+i),cmean(i)/cstd(i),dat(idat(8)+1+i),int(DAT(IDAT(7)+1+i))

  end do ! M Loop

  !=====================!
  ! Information Sharing !
  !=====================!

  do id=0,num_proc-1

     is   = idec*id + 1
     ie   = idec*(id+1)
     if(id.eq.num_proc-1)ie = M

     call MPI_BCAST(DAT(idat(6)+1+is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
     call MPI_BCAST(DAT(idat(7)+1+is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
     call MPI_BCAST(DAT(idat(8)+1+is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)

     call MPI_BCAST(cmean(is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
     call MPI_BCAST(cstd(is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)

     do i=1,N
        call MPI_BCAST(fmeanprimetmp(is,i),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(fvarprimetmp(is,i),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
     end do

!!$        do i=1,ndim
!!$           do j =1,ndim
!!$              call MPI_BCAST(d2fdata(is,i,j),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
!!$           end do
!!$        end do

  end do ! all the procs

!========================!
!      Assemble          !
!========================!

  do i =1,M

     G(i)=cmean(i)+dble(kprob)*cstd(i)

     do j=1,N
        dc(i,j)=fmeanprimetmp(i,j)
        if (fvartmp.ne.0.0) then
           dc(i,j)=dc(i,j)+dble(kprob)*fvarprimetmp(i,j)/(2.0d0*cstd(i))
        end if
     end do

     if (id_proc.eq.0) dat(idat(6)+1+i)=g(i)

  end do

  !===============!
  ! Just printing !
  !===============!
  
  if (id_proc.eq.0) then

     do i =1,M
        fgcnt=fgcnt+int(DAT(IDAT(7)+1+i))
        write(*,'(i4,3E15.6,F15.6,F8.1,i8)'),i,cmean(i),cstd(i),g(i),cmean(i)/cstd(i),DAT(IDAT(8)+1+i), int(DAT(IDAT(7)+1+i))
     end do

  end if

!  call MPI_Barrier(MPI_COMM_WORLD,ierr)

  !---- INEQUALITY CONSTRAINTS gradient

  do i=1,N
     DAT(3*N+2+i)= Xsave(i)
     X(i)        = Xsave(i)
  end do

  cnt=0
  do i=1,M
     do j=1,N
        cnt=cnt+1
        DAT(4*N+2+cnt)=dc(i,j)
        if (4*N+2+cnt.gt.idat(4)) stop"DAT Array corrupted"
     end do
  end do

  IERR = 0

  return
end subroutine EV_G

!
! =============================================================================
!
!                Computation of gradient of objective function
!
! =============================================================================
!
subroutine EV_GRAD_F(N, X, NEW_X, GRAD, IDAT, DAT, IERR)
  use dim_opt,only:id_proc,fcnt,fgcnt,fghcnt
  use timer_mod
  implicit none
  include 'mpif.h'
  integer N, NEW_X,i
  double precision GRAD(N), X(N), sigmax(N), fmeantmp, fvartmp
  double precision DAT(*),fmeanprimetmp(n),fvarprimetmp(n)
  real*8 :: fmeandbleprimetmp(n,n),fvardbleprimetmp(n,n)
  integer IDAT(*),kprob,NMC
  integer IERR
  logical samex
  integer::myflag(10) 
  real::tt
  integer::M

  M=idat(3)

  samex=.true.
  do i=1,N
     if (x(i).ne.DAT(2*N+2+i)) samex=.false. 
  end do

  if (samex) then

     if (id_proc.eq.0) print *,'Samex in obj',X

     !---- TOTAL GRADIENT OF OBJECTIVE FUNCTION

     do i=1,n
        GRAD(i)=DAT(N+2+i)
     end do

  else

     if (id_proc.eq.0) print *,'Not Samex in obj',X

     kprob=IDAT(1)

     do i=1,N
        sigmax(i)=DAT(i)
     end do

     !---- MEAN and VARIANCE OF worst OBJECTIVE FUNCTION

     if (id_proc.eq.0) then

        fcnt=0
        tt=0.0

        call TimerInit()
        call TimerStart('Build Kriging START')                
        call Krigingestimate(3,N,x,sigmax,24,0,dat(IDAT(5)+1:IDAT(6)),20,20,20,0,myflag,fmeantmp,fvartmp,fmeanprimetmp,fvarprimetmp)
        call TimerStop('Build Kriging END')
        call sendtime(tt)

        call system('mv HISTGidx000.dat krig/.')

        dat(idat(6)+1)=fmeantmp+fvartmp
        dat(idat(7)+1)=fcnt
        dat(idat(8)+1)=tt
     
  end if

  call MPI_BCAST(dat(idat(6)+1),1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(dat(idat(7)+1),1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(dat(idat(8)+1),1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  
  call MPI_BCAST(fmeantmp,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(fvartmp,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(fmeanprimetmp,N,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(fvarprimetmp,N,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

   if (IDAT(2).eq.1) then
        fvartmp=0.0
        fvarprimetmp(:)=0.0
     end if

     !---- OBJECTIVE FUNCTION gradient and x value

     do i=1,N
        grad(i)=fmeanprimetmp(i)+fvarprimetmp(i)
     end do
  end if

  IERR = 0
  return
end subroutine EV_GRAD_F

!
! =============================================================================
!
!                Computation of Jacobian of constraints
!
! =============================================================================
!
subroutine EV_JAC_G(TASK, N, X, NEW_X, M, NZ, ACON, AVAR, A,IDAT, DAT, IERR)
  use dim_opt,only:id_proc,fcnt,fgcnt,fghcnt,num_proc
  use timer_mod
  implicit none
  include 'mpif.h'
  integer TASK, N, NEW_X, M, NZ
  double precision X(N), A(NZ),dc(M,N), sigmax(N), fmeantmp, fvartmp
  integer ACON(NZ), AVAR(NZ), I, J, K, cnt, NMC
  double precision DAT(*),fmeanprimetmp(m,n),fvarprimetmp(m,n),cstd(M),cmean(M)
  real*8 :: fmeandbleprimetmp(n,n),fvardbleprimetmp(n,n)
  integer IDAT(*)
  integer IERR, kprob,jaccnt
  logical samex
  integer::myflag(10) 
  real::tt
  double precision ::gvec(m),cgrad(m,n-3)
  character(len=11)::chdirec
  integer :: idx,is,ie,idec,id
  character(len=3)::iNumber


  if( TASK.eq.0 ) then 

     jaccnt=0      
     do i=1,M
        do j=1,N
           jaccnt=jaccnt+1
           ACON(jaccnt)=i
           AVAR(jaccnt)=j
        end do
     end do

  else

     samex=.true.
     do i=1,N
        if (x(i).ne.DAT(3*N+2+i)) samex=.false. 
     end do

     if (samex) then

        cnt=0
        do i=1,M
           do j=1,N
              cnt=cnt+1
              dc(i,j)=DAT(4*N+2+cnt)
              if (4*N+2+cnt.gt.idat(4)) stop"DAT Array corrupted"
           end do
        end do


     else

        kprob=IDAT(1)

        do i=1,N
           sigmax(i)=DAT(i)
        end do

        dc(:,:)=0.0

        !====================================================!
        !================ WORK SHARE ========================!
        !====================================================!

        idec = dble(M)/dble(num_proc)
        is   = idec*id_proc + 1
        ie   = idec*(id_proc+1)
        if(id_proc.eq.num_proc-1)ie = M

        chdirec(1:8)="krig/con"

        do i=is,ie

           call i_to_s2(i,Inumber)
           chdirec(9:11)=inumber
           call chdir(chdirec)

           fcnt=0
           tt=0.0

           call TimerInit()
           call TimerStart('Build Kriging START')        
           call Krigingestimate(3,N,x,sigmax,24,i,dat(IDAT(5)+1:IDAT(6)),20,20,20,0,myflag,fmeantmp,fvartmp,fmeanprimetmp(i,1:n),fvarprimetmp(i,1:n))
           call TimerStop('Build Kriging END')
           call sendtime(tt)

           call system('mv HIST* ../.')
           call chdir('../../')

           DAT(IDAT(6)+1+i) = fmeantmp+dble(kprob)*sqrt(fvartmp)
           DAT(IDAT(7)+1+i) = fcnt ! store the # fn evals
           DAT(IDAT(8)+1+i) = tt   ! store the time

           if(fvartmp.lt.0.0) fvartmp=0.0
           if (IDAT(2).eq.1) then
              fvartmp=0.0
              fvarprimetmp(i,:)=0.0
           end if

           cmean(i)=fmeantmp
           cstd(i)=sqrt(fvartmp)

        end do ! is,ie

        !=====================!
        ! Information Sharing !
        !=====================!

        do id=0,num_proc-1

           is   = idec*id + 1
           ie   = idec*(id+1)
           if(id.eq.num_proc-1)ie = M

           call MPI_BCAST(cmean(is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
           call MPI_BCAST(cstd(is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
           
           call MPI_BCAST(DAT(idat(6)+1+is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
           call MPI_BCAST(DAT(idat(7)+1+is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
           call MPI_BCAST(DAT(idat(8)+1+is),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)

           do i=1,N
              call MPI_BCAST(fmeanprimetmp(is,i),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
              call MPI_BCAST(fvarprimetmp(is,i),ie-is+1,MPI_DOUBLE_PRECISION,id,MPI_COMM_WORLD,ierr)
           end do

        end do ! all the procs
        
        !=======================!
        !        Assemble       !
        !=======================!

        do i =1,M

           do j=1,N
              dc(i,j)=fmeanprimetmp(i,j)
              if (fvartmp.ne.0.0) then
                 dc(i,j)=dc(i,j)+dble(kprob)*fvarprimetmp(i,j)/(2.0d0*cstd(i))
              end if
           end do

        end do

     end if

!     call MPI_Barrier(MPI_COMM_WORLD,ierr)

     !================!
     !    Assemble    !
     !================!

     jaccnt=0      
     do i=1,M
        do j=1,N
           jaccnt=jaccnt+1
           A(jaccnt)=dc(i,j)
           if (4*N+2+jaccnt.gt.idat(4)) stop"DAT Array corrupted"
        end do
     end do

  end if ! task 0

  IERR = 0
  return
end subroutine EV_JAC_G
!
! =============================================================================
!
!                Computation of Hessian of Lagrangian
!
! =============================================================================
!
subroutine EV_HESS(TASK, N, X, NEW_X, OBJFACT, M, LAM, NEW_LAM,NNZH, IRNH, ICNH, HESS, IDAT, DAT, IERR)
  implicit none

  integer TASK, N, NEW_X, M, NEW_LAM, NNZH,  i,j,ii
  double precision X(N), OBJFACT, LAM(M), HESS(NNZH), sigmax(N)
  integer IRNH(NNZH), ICNH(NNZH)
  double precision::fmeantmp,fvartmp
  double precision OBJHESS(NNZH),CONHESS(M,NNZH)
  double precision DAT(*)
  integer IDAT(*), kprob
  integer IERR,hesscnt

  if( TASK.eq.0 ) then
     !
     !     structure of sparse Hessian (lower triangle):
     !
     hesscnt=0
     do i=1,N
        do j=1,i
           hesscnt=hesscnt+1
           IRNH(hesscnt)=j
           ICNH(hesscnt)=i
        end do
     end do

  else

     print*,"Should not have been here..."

     IERR = 1

  endif

  return
end subroutine EV_HESS



!
! =============================================================================
!
!                   Callback method called once per iteration
!
! =============================================================================
!
subroutine ITER_CB(ALG_MODE, ITER_COUNT,OBJVAL, INF_PR, INF_DU,MU, DNORM, REGU_SIZE, ALPHA_DU, ALPHA_PR, LS_TRIAL, IDAT,DAT, ISTOP)
  use dim_opt,only:id_proc,fcnt,fgcnt,fghcnt

  implicit none

  include 'mpif.h'

  integer ALG_MODE, ITER_COUNT, LS_TRIAL
  double precision OBJVAL, INF_PR, INF_DU, MU, DNORM, REGU_SIZE
  double precision ALPHA_DU, ALPHA_PR
  double precision DAT(*)
  integer IDAT(*),i
  integer ISTOP,ierr
  double precision::dnorm_tol, con_tol, mu_tol
  logical stop_now

  character(len=12)::chdirec
  integer :: idx,is,ie,idec,id
  character(len=3)::iNumber
  
  if (id_proc.eq.0) then

     !===============================
     !! First save the histograms
     !==============================

     chdirec(1:9)="hist/iter"
     call i_to_s2(iter_count,Inumber)
     chdirec(10:12)=inumber
     call chdir(chdirec)
     call system('mv ../../krig/HIST* .')
     call chdir('../../')

     write(*,*) 
     write(*,*) 'iter    objective      ||grad||        inf_pr          inf_du         lg(mu)    #current     #Total'

     fghcnt=fghcnt+ (fgcnt + int(DAT(IDAT(7)+1))) ! total computational expense TILL this iteration
     
     write(*,'(i5,5e15.7,2i8)') ITER_COUNT,OBJVAL,DNORM,INF_PR,INF_DU,MU, fgcnt+int(DAT(IDAT(7)+1)),fghcnt 
                                                                            ! cost at this iter      , totalcost
     write(716,'(i5,5e15.7,2i10)') ITER_COUNT,OBJVAL,DNORM,INF_PR,INF_DU,MU, fgcnt+int(DAT(IDAT(7)+1)),fghcnt

     write(816,'(i5,9999e15.7)') ITER_COUNT,OBJVAL,(dat(idat(6)+1+i),i=1,idat(3))

     if (ITER_COUNT.gt.15) then

        open(unit=30,file='stop.inp',status='old')
        read(30,*) dnorm_tol
        read(30,*) con_tol
        read(30,*) mu_tol
        read(30,*) stop_now
        close(30)

        if (inf_pr.le.con_tol .and. dnorm .le. dnorm_tol) ISTOP=1
        if (stop_now) istop=1

     end if !iter_count gt 1

  end if ! master

  fgcnt=0              ! reset every iteration

  if (iter_count.eq.30) istop=1
  
  call MPI_BCAST(istop,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

  return
end subroutine ITER_CB
